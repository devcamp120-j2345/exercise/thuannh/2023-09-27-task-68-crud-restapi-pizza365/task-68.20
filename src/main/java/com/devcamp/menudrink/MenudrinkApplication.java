package com.devcamp.menudrink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MenudrinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(MenudrinkApplication.class, args);
	}

}
