package com.devcamp.menudrink.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.menudrink.model.CMenu;
import com.devcamp.menudrink.repository.MenuRepository;
import com.devcamp.menudrink.service.MenuService;

@RestController
@RequestMapping("/menu")
@CrossOrigin
public class MenuController {
    @Autowired
    MenuService menuService;

    @GetMapping("/all")
    public List<CMenu> getAllMenu(){
        return menuService.getAllMenu();
    }

    @Autowired
    MenuRepository menuRepository;
    @PostMapping("/create")
	public ResponseEntity<CMenu> createCMenu(@RequestBody CMenu pMenu) {
		try {
			CMenu _menu = menuRepository.save(pMenu);

			return new ResponseEntity<>(_menu, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/update/{id}")
	public ResponseEntity<CMenu> updateCMenuById(@PathVariable("id") int id, @RequestBody CMenu pMenu) {
        try {
            CMenu menu = menuRepository.findById(id);

            if (menu != null) {
                menu.setCombo(pMenu.getCombo());
                menu.setDuongKinh(pMenu.getDuongKinh());
                menu.setSalad(pMenu.getSalad());
                menu.setSuonNuong(pMenu.getSuonNuong());
                menu.setThanhTien(pMenu.getThanhTien());
                return new ResponseEntity<>(menuRepository.save(menu), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
		
	}

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CMenu> deleteMenuById(@PathVariable("id") int id) {
		try {
			CMenu menu = menuRepository.findById(id);
            menuRepository.delete(menu);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

