package com.devcamp.menudrink.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "menu")
public class CMenu {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column (name = "combo", unique = true, nullable = false)
    private String combo;

    @Column (name = "duong_kinh", nullable = false)
    private String duongKinh;

    @Column (name = "suon_nuong", nullable = false)
    private String suonNuong;

    @Column (name = "salad", nullable = false)
    private String salad;

    @Column (name = "nuoc_ngot", nullable = false)
    private int nuocNgot;

    @Column (name = "thanh_tien", nullable = false)
    private double thanhTien;

    public CMenu(String combo) {
        this.combo = combo;
    }
    public CMenu(String combo, String duongKinh, String suonNuong, String salad, int nuocNgot) {
        this.combo = combo;
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
    }
    
    public CMenu() {
    }
    
    public CMenu(int id, String combo, String duongKinh, String suonNuong, String salad, int nuocNgot,
            double thanhTien) {
        this.id = id;
        this.combo = combo;
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.thanhTien = thanhTien;
    }
    public CMenu(String combo, String duongKinh, String suonNuong, String salad, int nuocNgot, double thanhTien) {
        this.combo = combo;
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.thanhTien = thanhTien;
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getCombo() {
        return combo;
    }
    public void setCombo(String combo) {
        this.combo = combo;
    }
    public String getDuongKinh() {
        return duongKinh;
    }
    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }
    public String getSuonNuong() {
        return suonNuong;
    }
    public void setSuonNuong(String suonNuong) {
        this.suonNuong = suonNuong;
    }
    public String getSalad() {
        return salad;
    }
    public void setSalad(String salad) {
        this.salad = salad;
    }
    public int getNuocNgot() {
        return nuocNgot;
    }
    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }
    public double getThanhTien() {
        return thanhTien;
    }
    public void setThanhTien(double thanhTien) {
        this.thanhTien = thanhTien;
    }
}
