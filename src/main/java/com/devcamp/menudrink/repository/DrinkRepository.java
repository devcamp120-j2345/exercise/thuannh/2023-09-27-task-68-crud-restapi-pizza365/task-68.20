package com.devcamp.menudrink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrink.model.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Long>  {
    
}
