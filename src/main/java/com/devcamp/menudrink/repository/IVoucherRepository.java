package com.devcamp.menudrink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrink.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}

