package com.devcamp.menudrink.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.menudrink.model.Drink;
import com.devcamp.menudrink.repository.DrinkRepository;


@Service
public class DrinkService {
    @Autowired
    DrinkRepository drinkRepository;
    public List<Drink> getAllDrinks(){
        List<Drink> drinkList = new ArrayList<Drink>();
        drinkRepository.findAll().forEach(drinkList::add);
        return drinkList;
    }

    public Drink getDrinkById(long id){
        Optional<Drink> drinkData = drinkRepository.findById(id);
        if (drinkData.isPresent()){
            Drink drink = drinkData.get();
            return drink;
        }
        else return null;
    }
}

