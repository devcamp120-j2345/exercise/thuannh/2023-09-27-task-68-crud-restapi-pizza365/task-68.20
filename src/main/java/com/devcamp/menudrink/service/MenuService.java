package com.devcamp.menudrink.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.menudrink.model.CMenu;
import com.devcamp.menudrink.repository.MenuRepository;

@Service
public class MenuService {
    //CMenu comboSmall = new CMenu("small","20cm","2","200gr",2,150000);
    //CMenu comboMedium = new CMenu("medium","25cm","4","300gr",3,200000);
    //CMenu comboLarge = new CMenu("large","30cm","8","500gr",4,250000);
    //public ArrayList<CMenu> getAllMenu(){
    //    ArrayList<CMenu> listMenu = new ArrayList<>();
    //    listMenu.add(comboSmall);
    //    listMenu.add(comboMedium);
    //    listMenu.add(comboLarge);
    //    return listMenu;
    //}
    @Autowired
    MenuRepository menuRepository;
    public List<CMenu> getAllMenu(){
        List<CMenu> listMenu = new ArrayList<CMenu>();
        menuRepository.findAll().forEach(listMenu::add);
        return listMenu;
    }

}

