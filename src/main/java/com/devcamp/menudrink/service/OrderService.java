package com.devcamp.menudrink.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.menudrink.model.COrder;
import com.devcamp.menudrink.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository orderRepository;
    public List<COrder> getAllOrders(){
        List<COrder> orders = new ArrayList<COrder>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }
}
