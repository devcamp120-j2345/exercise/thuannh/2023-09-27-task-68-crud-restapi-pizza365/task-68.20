package com.devcamp.menudrink.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.menudrink.model.CUser;
import com.devcamp.menudrink.model.COrder;
import com.devcamp.menudrink.repository.IUserRepository;

@Service
public class UserService {
    @Autowired
    IUserRepository userRepository;
    public ArrayList<CUser> getAllUsers(){
        ArrayList<CUser> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList:: add);
        return userList;
    }
    public Set<COrder> getOrderByUserId(long id){
        CUser vUser = userRepository.findById(id);
        if ( vUser != null){
            return vUser.getOrders();
        }
        else return null;
    }
}
